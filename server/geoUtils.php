<?php

/**
 * Calculate a new coordinate based on start, distance and bearing
 *
 * @param $start array - start coordinate as decimal lat/lon pair
 * @param $dist  float - distance in kilometers
 * @param $brng  float - bearing in degrees (compass direction)
 */
function geo_destination($start,$dist,$brng){
    $lat1 = toRad($start['latitude']);
    $lon1 = toRad($start['longitude']);
    $dist = $dist/6371.01; //Earth's radius in km
    $brng = toRad($brng);
 
    $lat2 = asin( sin($lat1)*cos($dist) +
                  cos($lat1)*sin($dist)*cos($brng) );
    $lon2 = $lon1 + atan2(sin($brng)*sin($dist)*cos($lat1),
                          cos($dist)-sin($lat1)*sin($lat2));
    $lon2 = fmod(($lon2+3*pi()),(2*pi())) - pi();  
 
    return array(toDeg($lat2),toDeg($lon2));
}
function toRad($deg){
    return $deg * pi() / 180;
}

function toDeg($rad){
    return $rad * 180 / pi();
}

/**
 * Retorna verdadedeiro se um ponto est� dentro de um quadrado definido por NE SW
 */
function inBounds($pointLong,$pointLat, $latNordeste,$longNordeste,$latSudoeste,$longSudoeste){
    //echo "\n inBounds($pointLong,$pointLat, $latNordeste,$longNordeste,$latSudoeste,$longSudoeste)";
    $eastBound = $pointLong < $latNordeste;
    $westBound = $pointLong > $longSudoeste;
    $inLong;

    if ($longNordeste < $longSudoeste) {
        $inLong = $eastBound || $westBound;
    } else {
        $inLong = $eastBound && $westBound;
    }

    $inLat = $pointLat > $latSudoeste && $pointLat < $latNordeste;
    //echo "\n return inLat && inLong = ".($inLat && $inLong).";";
    return $inLat && $inLong;
}

?>