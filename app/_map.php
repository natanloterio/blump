<?php require_once('sessao.php'); ?>

<?php
 include_once("login_util.php");
 include_once("../lib/Utils.php");
?>
<!DOCTYPE HTML>
<html lang="en-US">
    
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>Blump away</title>

<?php require_once('includes-basicos.php');?>

<link rel="stylesheet" href="../css/style.css"/>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/easywebsocket.js"></script>
<script src="js/User.js"></script>
<script src="js/Util.js"></script>
<script src="js/blumps.js"></script>

<script>
  var Server;
  var map = 0;
  var longitude = 0;
  var latitude = 0;
  var iconBase = '<?php echo curPageURL();?>/images/marker/marcador.png';
  var users = [];
  var userID = <?php echo getUsuarioLogadoID(); ?> ; //Math.floor(Math.random() * 100); //$("#userID").val();
  var down = {};
  var show_blumps = true;


  function show_position(position) {

      // atualiza no servidor a posição to sujeito
      Server.updateUserLocation(position);

  }


  function initialize_map(position) {

      // atualiza no servidor a posição to sujeito
      Server.updateUserLocation(position);

      var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var myOptions = {
	  zoom: 18,
	  center: latlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	  //scrollwheel: false,
	  //navigationControl: false,		    
	  mapTypeControl: false,
	  //scaleControl: false,
	  //draggable: false,		    
      }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      // pegar do  servidor uma lista dos usuarios dentro desses limites
      setInterval(function () {
	  var bounds = map.getBounds();
	  var ne = new Array();
	  var sw = new Array();

	  ne[0] = bounds.getNorthEast().lb;
	  ne[1] = bounds.getNorthEast().mb;
	  sw[0] = bounds.getSouthWest().lb;
	  sw[1] = bounds.getSouthWest().mb;
	  Server.whoIsHere(ne, sw); // who is here server?
      }, 3000);

      // define listener para sabermos quando os limites do mapa mudam
      google.maps.event.addListener(map, 'tilesloaded', function () {

      });


      // depois de renderizar o mapa, chama um evento. Adequado à este momento
      //addUser(userID, map, latlng); TODO : AGORA A POSICAO DESTE CARA VIRÁ DO SERVIDOR
      // VIDE EVENTO onUpdateUseresHere
  }

  function abrirDivSobreMarker() {
      var topRight = gmap.getProjection().fromLatLngToPoint(gmap.getBounds().getNorthEast());
      var bottomLeft = gmap.getProjection().fromLatLngToPoint(gmap.getBounds().getSouthWest());
      var scale = Math.pow(2, gmap.getZoom());
      var worldPoint = gmap.getProjection().fromLatLngToPoint(marker.getPosition());
      var point = new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
  }


  function abrirChat() {
      $("#popupBasic").popup("open");
  }

  function showBlumps() {
      largura = $('#map_canvas').width()
      $("#div_blumps").width(largura);
      
      altura = $('#map_canvas').height()
      $("#div_blumps").height(altura);
      
      buscarBlumps();
      
      
      $("#div_blumps").show();
    
  }
  

  function hideBlumps() {
    
    $("#div_blumps").hide();
    
  }  
  
  
  function addUser(userID, map, position) {
      usuario = new User(userID, map, position);
      users.push(usuario);
      var mrk = usuario.getMarker(userID);

  }


  function removeUserFromMap(userID) {
      // procura o usuario dentro do array de usuarios
      var len = users.length;
      for (var i = 0; i < len; i++) {
	  var uid = users[i].getUserID();
	  if (uid == userID) {
	      users[i].removeMarker();
	      users.remove(i);
	  }
      }

  }


  function send(receiver_userid, text) {
      Server.messageToUser(receiver_userid, text);
  }


  function conectar() {
      //var serverUrl = 'webserver-natan.zapto.org';
      //var serverUrl = '189.26.186.42';
      //var serverUrl = '192.168.1.4'; //$("#serverUrl").val();
      var serverUrl = '127.0.0.1';
      $('#userID').val(userID);

      Server = new EasyWebSocket('ws://' + serverUrl + ':843', userID);

      //Binda uma funcao ao evento onUserConnecting
      Server.bind('onUserConnecting', function (action) {
	  // TODO
      });

      //Binda uma funcao ao evento onReceiveMessage
      Server.bind('onReceiveBlump', function (action) {
	  buscarBlumps();
      });


      //Binda uma funcao ao evento onReceiveMessage
      Server.bind('onUpdateUseresHere', function (listaUsuarios) {
	  //log('onUpdateUseresHere');
	  var len = listaUsuarios.length;



	  // Insere 
	  for (var i = 0, len; i < len; i++) {
	      var bolEstaNoMapa = false;
	      var uid = listaUsuarios[i].userID;
	      var lat = listaUsuarios[i].position.latitude;
	      var lng = listaUsuarios[i].position.longitude;
	      var position = new google.maps.LatLng(lat, lng, false);

	      // se o usuario existe e sua posiçao permanece a mesma, atualiza
	      for (var j = 0; j < users.length; j++) {
		  // se o usuario está n array
		  if (users[j].getUserID() == uid) {
		      bolEstaNoMapa = true;
		      // verifica se su posição mudou
		      uLatChange = users[j].position.latitude == listaUsuarios[i].position.latitude;
		      uLonChange = users[j].position.longitude == listaUsuarios[i].position.longitude;
		      // se houve diferenca, remove marcador da posicao
		      if (uLatChange && uLonChange) {
			  removeUserFromMap(uid);
			  addUser(uid, map, listaUsuarios[i].position);
		      }


		  }
	      }

	      if (!bolEstaNoMapa) {
		  // se ele não estiver na lista, insere
		  addUser(uid, map, position);
	      }
	  }

	  // agora faz o inverso. Procura dentro da lista de enviados por cada usuario que já desenhamos.
	  // e check each of them aren't present in this new viewport.

	  for (i = 0; i < users.length; i++) {
	      userPresent = false;
	      uid = users[i].getUserID();
	      //search by this user in new Users in the list given by server
	      for (j = 0; j < listaUsuarios.length; j++) {
		  if (uid == listaUsuarios[j].userID) {
		      userPresent = true;
		  }
	      }

	      if (!userPresent) {
		  removeUserFromMap(uid);
	      }

	  }




      });

      //Binda uma funcao ao evento onReceiveMessage
      Server.bind('afterConnectionEstabilished', function (action) {
	  if (navigator.geolocation) {
	      // pega a posição inicial do usuario
	      navigator.geolocation.getCurrentPosition(initialize_map);
	      // chamdo quando a posição do usuario mudqa
	      navigator.geolocation.watchPosition(show_position)

	  }
      });

      Server.connect();

  }

  function initialize_app() {
      // conecta ao servidor
      conectar();

  }




  $(document).delegate("#mapa", "pageinit", function () {
      // pra ter certeza que os blumps não estarão visiveis
      hideBlumps();
   
   
      // inicia a aplicacao
      initialize_app();

      $('#titulo').on('click', function () {
	  $( "#menu_panel" ).panel( "open" );
       
      });
      
      
      $('#new_blump').on('click', function () {
       abrirChat();
       
      });
      
      // binda evendo botão novo blump
      $('#btn_blumps').on('click', function () {
       if (show_blumps) {
	  showBlumps();
       }else{
	  hideBlumps();
       }
       show_blumps = !show_blumps;
       
      });

      $("#textoaenviar").keydown(function (event) {
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  if (keycode == '13') {
	      if (down['13'] == null) { // first press
		  var mensagem = $("#textoaenviar").val();
		  Server.blump(-1, mensagem);

		  // limpa valor do input
		  $("#textoaenviar").val('');

		  $("#popupBasic").popup("close");
		  down['13'] = true; // record that the key's down
	      }
	  }
      });

      $("#textoaenviar").keyup(function (event) {
	  var keycode = (event.keyCode ? event.keyCode : event.which);
	  down[keycode] = null;
      });
  });  
	  

</script>

<style>
 /* Palette color codes */
/* Feel free to copy&paste color codes to your application */

.primary { background-color: #1C5380 }

.secondary-a { background-color: #223E85 }

.secondary-b { background-color: #147378 }

#div_blumps{   
 background: white;
 border: solid;
 width: 200px;
 height: 100px; 
 position: relative;
}

 

.likes{
float: right;
padding: 10px;

 }

#resultados{
    margin:0 auto;
    width:270px;
    height:200px;
    padding-top: 10px;
}

.item_linha{
list-style-type: none; 
width: 270px;

}

.item_div{
margin: 10px;
width: 270px;
border: solid 1px;
border-radius: 3px;
height: 50px;
padding: 3px;
background-color: rgb(235, 235, 235);
}
 
.item_div_user_picture{
 height: 50px; 
 width: 50px;
 float: left;
 background-color:rgb(238, 238, 238);
}

.item_div_mensagem{
 height: 50px;
 background-color:rgb(238, 238, 238);
}


.btn {
 color: blue; 
 border: 2px solid black;
font-size: 30px;
font-family: fantasy;
text-align: center;
cursor: pointer;
 } 
 

.up{
  top:22px;
 
 } 
 
  
.down{
 
 }

 
 .conteudo{
  float: left;
  padding: 10px;
 }
 .bloco{
    background: rgba(255, 255, 255, .3);
    border-color: rgba(255, 255, 255, .6);
    border-style: solid;
    border-width: 1px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
 ul#display-inline-block-example,
 ul#display-inline-block-example li {
	 /* Setting a common base */
	margin: 5px;
	padding: 5px;
 }

 ul#display-inline-block-example li {
	 display: inline-block;
	 background: #ccc;
	 vertical-align: top;
	 
	 /* For IE 7 */
	 zoom: 1;
	 *display: inline;
 }
 
 
.block {
    position: absolute;
    background: #eee;
    padding: 20px;
    /*width: 300px;*/
    border: 1px solid #ddd;
	/*-webkit-transition: all 1s ease-in-out;
	-moz-transition: all 1s ease-in-out;
	-o-transition: all 1s ease-in-out;
	-ms-transition: all 1s ease-in-out;
	transition: all 1s ease-in-out;
	*/

	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;    
}

blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

#div_location{
 width: 100%;
}
 
#div_location DIV{
left: 33%;
width: 400px;
}
 
</style>
	

		
</head>    
<body>
 
<!-- Inicio da pagina do mapa-->
<div id="mapa" data-role="page">
<!-- Menu lateral esquerda-->
<?php include('menu-lateral.php'); ?>
<!-- /panel -->	
<!-- Inicio cabecalho da pagina -->
  <div data-role="header">
   <!-- 
   <div id="div_location">    
        <input type="search" name="search" id="search-header" value="" data-mini="true" data-inline="true" data-theme="c" />
   </div>
   -->
    <a id="new_blump" href="#" data-role="button" data-theme="a">
    Blump oned Rock!</a>
    
    <h1 id="titulo">Blump</h1>
    
    <a id="btn_blumps" href="#" data-role="button" data-icon="menu" data-theme="a">
    Blumps</a>
    
  </div>
    <!-- Fim cabecalho  -->
	
	
	<!-- Inicio conteudo -->
	<div id="div_conteudo" data-role="content" class="content" style="width:100%; height:100%; padding:0;">
	  
	  <div id="wraper">
	 
		<div id="map_canvas">
		</div>
	 
		<div id="div_blumps">
		</div>
		
	  </div>
		
		<div data-role="popup" id="popupBasic" data-transition="flip">
			<div id="mensagem">
				<input id="textoaenviar" type="text" placeholder="Digite e pressione Enter"/>
			</div>
		</div>
	</div>
	<!-- Fim conteudo -->
</div>
<!-- Fim da pagina do mapa-->
</body>

</html>