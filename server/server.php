<?php
//error_reporting(E_ALL);
/*
 * Este arquivo implementa o protocolo de comunica��o do websocket
 * Os pacotes recebidos s�o chamados de Actions. Essas actions contem os seguintes
 * atributos:
 *
 *  action_id : contem uma chave unica para cada action (alfanumerico)
 *  type : tipo da action. Vide {@action}
 *  time : data de criacao do pacote no formato numerico sequencial
 *  sender : user_id de quem envia
 *  receiver : user_id do destinat�rio
 *  data : pacote contendo a mensagem
 *
 *  Abaixo segue um modelo de um Action em JSON
 *
 *
 *  {
 *  	action_id : df877899df,
 *  	type : USER_MESSAGE,
 *  	time : 1368231308432,
 *  	sender : df09234234,
 *  	receiver : kl44334434,
 *  	data : {msg:"Oi"}
 *
 *  }
 */
require_once("../app/connection.php");
include_once("ServerInterface.php");
include_once("geoUtils.php");
include_once("../app/geo_database.php");
require_once("../app/blump_util.php");
//clude_once (dirname(__FILE__) . "/../../app/model/US_USERNOTIFYCheck.php");
//clude_once (dirname(__FILE__) . "/../../app/model/US_USERCheck.php");
//clude_once (dirname(__FILE__) . "/../../app/model/banco/connection.php");
//include_once (dirname(__FILE__) . "/../../app/lib/sessions.php");
//clude_once (dirname(__FILE__) . "/../../app/lib/loginutils.php");
//mostra();
// prevent the server from timing out
set_time_limit(0);

// include the web sockets server script (the server is started at the far bottom of this file)
require 'class.PHPWebSocket.php';

// array com a relacao usuario x socket
$usuariosConectados = array();
// array com o relacionamento clientID x UserID
$relUserNick = array();
$relClientIDvsUserID = array();
$relUserIDvsClientID = array();
$relUsuarioVsContato = array();
// rela��o0 de usuarios dentro de suas respectivas coordenadas
$relUsuariosVsLatitudeLongitude = array();
$relUsuariosVsLongitudeLatitude = array();

$maxUsers = 0;
$maxSockets = 0;

// Matriz com o ID do usu�rio e os sockets deste.
//$relUserIDvsClientID = array();


/**
 *  Constantes dos evnetos
 */
// Esse meu querido, � Deuxxxx .. Muaaa-ha-ha!
// thick-thick-thick-thick-thick-thick
const GOD = 1;
//Usuario est� se conectando
const USER_CONNECTING =13;
// Usu�rio est� se autenticando
const USER_AUTENTICATION = 14;
//Usu�rio est� desconectando
const USER_DISCONNECTING = 15;
// Mensagem direcionada a um usuario
const MESSAGE_TO_USER = 16;
// Um usuario mudou de status
const USER_STATE_CHANGE = 17;
// O usuario nao recebeu a mensagem
const MESSAGE_NOT_SENT_TO_USER = 23;
// O contato est� se conectando
const CONTACT_CONNECTING = 18;
// O contato est� se desconectando
const CONTACT_DISCONNECTING = 22;	
// registrando sua localizacao
const REGISTER_POSITION = 20;
// adiciona um contato
const ADD_CONTACT = 21;
// Eventos gerados pelo usuario
const USER_INTERN_ACTION = 24;
// atualizando localizacao de um usuario
const UPDATE_USER_LOCATION = 20;
// registrando sua localizacao
const WHO_IS_HERE = 25;
// Usuario logou
const BLUMP = 26;
// Atualiza nome de usuario
const UPDATE_NICKNAME = 27;

$ACTIONS_TRANSLATOR = array(
				GOD => 'GOD',
				USER_CONNECTING => 'USER_CONNECTING',
				USER_AUTENTICATION => 'USER_AUTENTICATION',
				USER_DISCONNECTING => 'USER_DISCONNECTING',
				MESSAGE_TO_USER => 'MESSAGE_TO_USER',
				USER_STATE_CHANGE => 'USER_STATE_CHANGE',
				MESSAGE_NOT_SENT_TO_USER => 'MESSAGE_NOT_SENT_TO_USER',
				CONTACT_CONNECTING => 'CONTACT_CONNECTING',
				CONTACT_DISCONNECTING =>	'CONTACT_DISCONNECTING',
				REGISTER_POSITION => 'REGISTER_POSITION',
				ADD_CONTACT => 'ADD_CONTACT',
				USER_INTERN_ACTION => 'USER_INTERN_ACTION',
				UPDATE_USER_LOCATION => 'UPDATE_USER_LOCATION',
				WHO_IS_HERE => 'WHO_IS_HERE',
				BLUMP => 'BLUMP',
				UPDATE_NICKNAME => 'UPDATE_NICKNAME'
			    );

/**PORTA DE ENTRADA DO SERVIDOR
 * Quando um cliente envia uma action para o server, lemos o cabecalho dessa action
 * e efetuamos a action que nela est� contida
 */
 
 // retorna true se este usuario possui o socket indicado
 function isAuthentic($clientID,$user){
		//echo "\n verificando autenticidade : socket=$clientID;sender:$user";
		$ownerOfSocket = getUserIDByClientID($clientID);
		//echo "\n dono do socket($clientID):$ownerOfSocket \n requisitante:$user";
		// se nao tem dono
		if(!$ownerOfSocket){
		  // ent�o o socket est� livre para conexao
			return true;
		}
		// se alguem j� possui
		// retorna true se as strings forem iguais 
		return (strcmp($ownerOfSocket,$user) === 0);
 
 }
 
function wsOnReceiveAction($clientID, $action, $messageLength, $binary) {
	global $Server;
	$Server->log("Servidor Recebeu A Action : \n".identaAction($action));
	// intercepta a mensagem e identifica o usuario nela presente
	$arAction = json_decode($action,true);
	//$Server->log($bundle);
	// pega o userid do sender
	$sender = $arAction['sender'];
	$receiver = $arAction['receiver'];		
	// adiciona o nick name na mensagem
	$arAction['nick'] = getNick($sender);


	// se n�o for autentica encerra chamada
	if(!isAuthentic($clientID,$sender)){
		echo "\n tentativa de autencicacao invalida $clientID,$sender";
		return;
	}
	
	// vincula um userid a um client id
	setUserIDByClientID($clientID, $sender);
	
	// tipo do evento
	$type = $arAction["type"];
	
	//echo "\n receiver:".$receiver."\n"." e type :".$type;

	// Separo as solicita��es para mim ( GOD ) das destinadas aos outros usuarios
	if($receiver == GOD){
		switch ($type){
			// caso seja um usuario se conectando
			case USER_CONNECTING:
					$nick = $arAction['nick'];
					// bindando o usuario ao clientid
					setUserIDByClientID($clientID, $sender);
					// adicionando um clientID a um usuario
					addClientIDToUser( $sender, $clientID);
					// set nick
					setUserNick($sender,$nick);
					// cria uma nova sessao
					$SESSAO = uniqid();
					
					// dispara evendo de um usuario se conectando				
					wsOnUserConnect($sender);
					
					$totConexoesAtivas = getTotalActiveConnections($sender);
					

				break;
			case BLUMP:
					$nick = getUserName($sender);
					$message = $arAction['message'];
                                        $blump_original = $arAction['blump_original'];
                                        // grava a nova posi��o do usuario
					$arrAction = array("action_id"=>$arAction["action_id"],
									"type"=>BLUMP,
									"sender"=>GOD,
									"nick"=>$nick,
									"message"=>$message,
                                                                        );
									
					// converto o array para json
					$newaction = json_encode($arrAction,true);
                                        				
					sendActionToUserContacts($sender,$newaction);
                                        
                                                // grava o blump no banco de dados
                                        $ultima_posicao = pegaUltimaPosicaoUsuario($sender);
                                        if(is_array($ultima_posicao)){
                                            echo json_encode($ultima_posicao);
                                            salvarBlump($blump_original,$sender,$arrAction['message'],$ultima_posicao['lat'],$ultima_posicao['lng']);
                                        }
				
			
			break;
			
			case UPDATE_NICKNAME:
					
					$newNick = $arAction['newNick'];
					"\n Atualizando NICK NAME:".$newNick;
					setUserNick($sender,$newNick);
					
					// grava a nova posi��o do usuario
					$arrAction = array("action_id"=>newActionID(GOD),
									"type"=>UPDATE_NICKNAME,
									"sender"=>GOD,
									"receiver"=>$sender,
									"newnick"=>$newNick);
					// converto o array para json
					$newaction = json_encode($arrAction,true);		
					sendActionToUser($newaction,GOD,$sender);				
				
			
			
			break;
			case UPDATE_USER_LOCATION:
				$newLatitude = strval($arAction['latitude']);
				$newLongitude = strval($arAction['longitude']);
				// grava a nova posi��o do usuario
				updateUserLocation($sender,$newLatitude,$newLongitude);
                            
				
				break;

			case WHO_IS_HERE:				
				// o usuario est� perguntando quem est� por perto dele
                                echo "\n WHO_IS_HERE valor do action recebido pelo server:".json_encode($arAction);
				$sw = $arAction['sw'];
				$ne = $arAction['ne'];
				$theyAreArroundYou = getUsersInsideThisBoundaries($ne,$sw);
				echo '\n variavel:$theyAreArroundYou ='.json_encode($theyAreArroundYou);
				echo "\n getUsersInsideThisBoundaries:".implode(",",$theyAreArroundYou);
				
				/**
				 * ATEN��O, ISSO � TEMPORARIO.
				 * INSERIMOS O PROPRIO USUARIO NA LISTA DE PESSOAS DENTRO DAS BARREIRAS
				 * ASSIM, FICA O SERVIDOR COMO O RESPONSAVEL POR DIZER QUAIS USUARIOS ( INCLUSIVE EU )
				 * QUE SERAO VISIVEIS DENTRO DOS LIMITES 
				 **/
				$arSender = array($sender);
				$theyAreArroundYou = $theyAreArroundYou + $arSender;

				// busca na lista de posi��es de usuarios
				global $usuariosConectados;
				$userWithPositions = array();
				
				$i = 0;
				foreach($theyAreArroundYou as $user){
					if(array_key_exists($user,$usuariosConectados)){

						$userWithPositions[$i] = array('userID' => $user,
									       'position' => $usuariosConectados[$user]['position']
									      );
						$i++;
						
					}

				}
				// grava a nova posi��o do usuario
				$arrAction = array("action_id"=>newActionID(GOD),
								"type"=>WHO_IS_HERE,
								"sender"=>GOD,
								"receiver"=>$sender,
								"people_arround"=>$userWithPositions);
				// converto o array para json
				$newaction = json_encode($arrAction,true);		
				sendActionToUser($newaction,GOD,$sender);				
				
				break;			
		}
	}else{
		/**
		 * Eventos de um usuario para outro
		 */

		// enviamos uma mensagem de volta para o usuario para avisar outros clientes deste mesmo usuario
		//wsOnUserIternAction($action);
			  
	    // adiciona o apelido do usuario na mensagem
		
			  
		// envia mensagem para o osusario
		sendActionToUser($action,$sender,$receiver);				
				
	}

}


function wsOnUserIternAction($action){

	$arAction = json_decode($action,true);
	//$Server->log($bundle);
	// pega o userid do sender
	$sender = $arAction['sender'];
	
	$arrUserInternAction = array("action_id"=>newActionID($sender),
				"type"=>USER_INTERN_ACTION,
				"sender"=>GOD,
				"receiver"=>$sender,
				"user_inter_action"=>$arAction);
	// converto o array para json
	$newaction = json_encode($arrUserInternAction,true);		
	sendActionToUser($newaction,GOD,$sender);

}

// when a client connects
function wsOnOpen($clientID){
	// 	global $Server;

}

// when a client closes or lost connection
function wsOnClose($clientID, $closeStatus) {
	// chamando onClose
	//echo "\n wsOnClose chamado";
	global $Server;
	// antes de remover o client, dispara um evento de desconxao do usuario deste ID
	$userid = getUserIDByClientID($clientID);
	if(!$userid){
	}else{
		$Server->log("\n fechando conexao com o cliente ".$clientID." do usuario ".$userid);
	}
	

}

function addContactToUser($user,$contact){
	
	global $relUsuarioVsContato;
	$relUsuarioVsContato[$user][$contact] = $contact;
	
	
}

function updateUserLocation($user, $latitude, $longitude){
	global $usuariosConectados;
	// salva a posi��o atual do usuario
	echo "\n usuario:".$user." lat:".$latitude." long:".$longitude;
	$usuariosConectados[$user]['position'] = array('latitude'=>$latitude, 'longitude'=>$longitude);
	bindUserToLocation($user,$latitude,$longitude);
	
	/**
	* agora preciso saber todas pessoas que est�o "vendo" a nova posi��o desse usuario.
	* 
	**TODO: usar um banco de dados geoespacial para armazenar estes valores
	*/
        salvarPosicaoUsuario($user,$latitude,$longitude);
}

function bindUserToLocation($user,$latitude,$longitude){
	global $relUsuariosVsLatitudeLongitude;
	//echo "\n bindUserToLocation($user,$latitude,$longitude)";
	// preciso salvar a posi��o deste usuario em 2 ararys
	// um mantem uma rela��o Latitude x Longitude de usuarios
	// a outra tem a relal��o Longitude x Latitude
	// assim, facilitar� para fazer buscas em um intervalo de longitude ou longitude quando necess�rio
	unset($relUsuariosVsLatitudeLongitude[$latitude][$longitude][$user]);
	$relUsuariosVsLatitudeLongitude[$latitude][$longitude][$user] = $user;
	//$relUsuariosVsLongitudeLatitude[$longitude][$latitude][$user] = $user;
	// now I should tell the 
	//echo "\n ravou usuario: ".$relUsuariosVsLatitudeLongitude[$latitude][$longitude][$user] ;
}

/**
 * Retorna um array com os usuarios que est�o dentro dos limites de um quadrado
 * definido por dois pontos no mapa. Um � o canto Noroeste do quadrado, e o outro
 * o canto Sudeste do quadrado
 **/
function getUsersInsideThisBoundaries($NE,$SW){
	global $relUsuariosVsLatitudeLongitude;
	echo "\n variavel NE:".json_encode($NE);
	$latNordeste = $NE[0];
	$longNordeste = $NE[1];
	$latSudoeste = $SW[0];
	$longSudoeste = $SW[1];
	
	$lstUsuarios = array();
	
	echo "\n getusersInsideThisBoundaries($latNordeste,$longNordeste,$latSudoeste,$longSudoeste)";
        echo "\ array con as posi��o dos usuarios vc sua posicao:".json_encode($relUsuariosVsLatitudeLongitude);
	
	// percorre todas as latitudes 
	foreach($relUsuariosVsLatitudeLongitude as $latitude => $arrLongitude){

		// percorre todas as longitues e pega o array de usuarios em cada posicao
		foreach($arrLongitude as $longitude => $usersHere){
			// verifica se a latitude est� nos limites			
			//echo "\n inBounds($longitude,$latitude, $latNordeste,$longNordeste,$latSudoeste,$longSudoeste);";
			$isInside = inBounds($longitude,$latitude, $latNordeste,$longNordeste,$latSudoeste,$longSudoeste);
			// com o array de usuarios em m�os, testamos cada um deles e vemos se est�o
			// dentro dos limites do quadrado
			if($isInside == 1){
				// verifico se existe um array e se este est� populado
				//echo "\n count(usersHere) = ".count($usersHere);
				if(count($usersHere) > 0){
					//array_merge(c);
					$lstUsuarios = $lstUsuarios + $usersHere;
				}
			}

		}
	}
	//echo "\n count(lstUsuarios) = ".count($lstUsuarios);
	return $lstUsuarios;
}

	



function updateFollowersList($user){
	
	$xlist = getFollowersList($user);
	
	foreach ( $xlist  as $follower){	

		addContactToUser($user,$follower);
	}
	
}

/**
* Retorna um array com os UserID's dos usuarios a serem notificados pelo usuario passado por parametro
*
*/
function getUserFollowers($user){
	global $usuariosConectados;
	$usuariosAoRedor = array();
	// retornar lista com pessoas nessa area
	if(array_key_exists($user,$usuariosConectados)){
		$userPosition = $usuariosConectados[$user]['position'];
		//echo "\n ".arrayToString($userPosition);
		$NE = geo_destination($userPosition,1,45);
		$SW = geo_destination($userPosition,1,225);
		
		$usuariosAoRedor = getUsersInsideThisBoundaries($NE,$SW);
	}
	//global $relUsuarioVsContato;
	//
	//if(array_key_exists($user,$relUsuarioVsContato))	{
	//
	//	return $relUsuarioVsContato[$user];
	//
	//}else{
	//	//updateFollowersList($user);
	//	return $relUsuarioVsContato[$user];
	//}
	return $usuariosAoRedor;
}

/**
 *
 * Retorna um novo e unico ActionID
 * @param unknown_type $userID
 */
function newActionID($userID){

	$milliseconds = round(microtime(true) * 1000);
	$actionID = $userID.$milliseconds;
	return $actionID;
}

/**
* Envia um action para os contatos de uma pessoa
*
*
*/
function sendActionToUserContacts($sender,$action){
	//echo "\n sendActionToUserContacts(sender=".$sender.") \n";
	$contatosDoUsuario = getUserFollowers($sender);
	$arrAction = json_decode($action,true);
	global $Server;
	
	// se a lista retornar alguma coisa
	if($contatosDoUsuario){
	
	foreach ( $contatosDoUsuario  as $contato){
			$arrAction['receiver'] = $contato;
			$act = json_encode($arrAction,true);
			// envia o action para o contato, =) simples !
			//echo "\n avisando contato (".$contato.") que usuario :".$sender." enviou action: ".$action."\n";
			sendActionToUser($act,$sender,$contato);
			
	
	
		}
	
	
	}
	/*else{
		$arrAction = array("action_id"=>newActionID(GOD),
						"type"=>MESSAGE_NOT_SENT_TO_USER,
						"sender"=>GOD,
						"receiver"=>$sender,
						"message"=>"Usuario n�o possui contatos online.",
						"data"=>$action);
		// converto o array para json
		$newaction = json_encode($arrAction,true);		
		sendActionToUser($newaction,GOD,$sender);
		
	}*/
        

                 
	
}


/**
*Envia um actoin para um usuario.
*Parametros:
* $action : action no formato array
* $sender : pesso que est� enviando
* $receiver : destino do action
* OBS.:Soh funciona para usuarios que se autenticaram no servidor ( bug )
* 
*
*/
function sendActionToUser($action, $sender, $receiver){
	global $Server;
	$Server->log("$sender enviou a action para $receiver : \n".identaAction($action));

	$arAction = json_decode($action,true);
	$type = $arAction['type'];
	
	// Certifica-se de enviar o nick de quem est� enviando
	$arAction['nick'] = getNick($sender);
	
	
	// Se as mensagens sao destinadas � Mim, as intercepto aqui

		// pego uma lista com todos os ids relacionados a este usuario
		$recClientIDs = getClientIDSByUser($receiver);
		$Server->log("Receiver :".$receiver);
		//$Server->log("recClientIDS :".$recClientIDs);
		// se o usuario possui alguns sockets abertos, enviamos para todos eles a mensagem
		if($recClientIDs){
			//$Server->log("passou no if recClientIDS");
			// e fa�o um loop enviando uma copia da mensagem para cada um deles ;) sussi
			foreach ( $recClientIDs as $id ){
					$Server->wsSend($id, $action);
			}

		}else{

			$arrAction = array("action_id"=>newActionID(GOD),
							"type"=>MESSAGE_NOT_SENT_TO_USER,
							"sender"=>GOD,
							"receiver"=>$sender,
							"message"=>"O usuario estava offline.",
							"data"=>$action);
			// converto o array para json
			$action = json_encode($arrAction,true);
			
			echo "".identaAction($action);
			
			// envio esse json de volta para o usuario que tentou enviar a mensagem
			$recClientIDs = getClientIDSByUser($sender);
			if($recClientIDs){
				// e fa�o um loop enviando uma copia da mensagem para cada um deles ;) sussi
				foreach ( $Server->wsClients as $id => $client )
					$Server->wsSend($id, $action);
			}
		}
	

}

function addClientIDToUser($UserID, $IDSocket){

	//echo "\n adicionando o ClientID $IDSocket ao usuario $UserID";

	global $relUserIDvsClientID;
	global $Server;
	global $xCountUsers;
	global $xCountSockets ;
	global $maxUsers;
	global $maxSockets;
	
	$relUserIDvsClientID[$UserID][$IDSocket] =  $IDSocket;

	$xCountUsers = count($relUserIDvsClientID);
	$xCountSockets = count($relUserIDvsClientID ,COUNT_RECURSIVE) - $xCountUsers;
	

	if ($xCountUsers > $maxUsers)
	$maxUsers = $xCountUsers;
	if ($xCountSockets > $maxSockets	)
	$maxSockets = $xCountSockets;

	$Server->log('Usuarios conectados  : '. $xCountUsers);
	$Server->log('Sockets instanciados : '. $xCountSockets);
//	$Server->log('Maximo de usuarios conectados : '. $maxUsers);
//	$Server->log('Maximo de sockets instanciados: '.$maxSockets);

	$Server->log('array de uduarios e sockets \n'.arrayToString($relUserIDvsClientID));
}

function removeClientIDFromUser($userID,$clientID){
	global $Server;
	global $relUserIDvsClientID;
	//if(array_key_exists($userID,$relUserIDvsClientID)){
	// remove o socket do array
        if(isset($relUserIDvsClientID[$userID][$clientID]))
            unset($relUserIDvsClientID[$userID][$clientID]);
	// se o usuario n�o possui mais sockets
	if(count($relUserIDvsClientID[$userID]) == 0)
		// remove o usuario da lista tb
		unset($relUserIDvsClientID[$userID]);
	
	//}
	

}
function removeUserFromPosition($user){
	global $relUsuariosVsLatitudeLongitude;
	global $usuariosConectados;
	if(array_key_exists($user,$usuariosConectados)){
		$userLat = $usuariosConectados[$user]['position']['latitude'];
		$userLon = $usuariosConectados[$user]['position']['longitude'];
		
		unset($usuariosConectados[$user]);
		if(array_key_exists($user,$relUsuariosVsLatitudeLongitude)){
			unset($relUsuariosVsLatitudeLongitude[$userLat][$userLon][$user]);
		}		
	}

	
}
function removeUserFromClientIDs($clientID){
	global $relClientIDvsUserID;
	unset($relClientIDvsUserID[$clientID]);
}

// retorna um array com a lista dos clientIDs de um usuario
function getClientIDSByUser($userid){
	global $Server;
	global $relUserIDvsClientID;
	$cliIDS = array();
	// returnoando um array de clientsIDs
	//$Server->log("getClientIDSByUser(".$userid.");");
	if(array_key_exists($userid,$relUserIDvsClientID)){
		$cliIDS = $relUserIDvsClientID[$userid];
		//$Server->log("primeiro id do usuario ".$userid." = ".$cliIDS);
	}
	return $cliIDS;
}



function getUserIDByClientID($clientID){
	global $relClientIDvsUserID;
	global $Server;
	
	// se o id existe
	// pega o aaray
	if(array_key_exists($clientID,$relClientIDvsUserID)){
		return $relClientIDvsUserID[$clientID];
	}
	//$Server->log("\n relClientIDvsUserID[".$clientID."] = ".$relClientIDvsUserID[$clientID]);

	return false;
}

function setUserIDByClientID($clientID, $userID){
	global $relClientIDvsUserID;
	global $Server;
	//$Server->log("\n setUserIDByClientID(".$clientID.",".$userID.");");
	$relClientIDvsUserID[$clientID] = $userID;
}

function setUserNick($user,$newNick){
	global $relUserNick;
	$relUserNick[$user] = $newNick;
	
}

function getNick($user){
	global $relUserNick;
	if(isset($relUserNick[$user] )){
		return $relUserNick[$user] ;
	}else{
		return "nenhumnick";
		
	}
	

}

function arrayToString($array,$spaces=1){
  global $ACTIONS_TRANSLATOR;
  $spaces++;
  $espacos = str_pad("",$spaces);	
  $outval = '';
  foreach($array as $key=>$value)
    {
    if(is_array($value))
      {
      $outval .= "\n |$espacos $key";
      $outval .= arrayToString($value,$spaces);
      }
    else
      {
	if(strcmp($key,"type") == 0 ){$outval .= "\n |$espacos $key : $ACTIONS_TRANSLATOR[$value]";}
	else
	{$outval .= "\n |$espacos $key : $value";}
      
      }
    }
  return $outval;
	
}
function identaAction($action){
	
	$arrAction = json_decode(strval($action),true);
	
	$actionIdentado = "\n ======== ACTION[".$arrAction['action_id']."]================";
	
	$actionIdentado .=  arrayToString($arrAction);
	
	$actionIdentado .= "\n =========================================== \n";
	
	return $actionIdentado;

}
/**
 *
 * Chamado antes de chamar o evento onClose no cliente
 * @param unknown_type $clientID
 * @param unknown_type $status
 */
function wsOnClientDisconnect($clientID,$status){
	echo "\n wsOnClientDisconnect(clientID=".$clientID.",".$status.")";
	// descobre o codigo do usuario
		$uid = getUserIDByClientID($clientID);
		// remove este socket da lista;
		removeClientIDFromUser($uid,$clientID);
		removeUserFromClientIDs($clientID);
		removeUserFromPosition($uid);

		
		// verifica se ainda possuimos algum outro socket conectado a esse usuario
		$totConexoesAtivas = getTotalActiveConnections($uid);
		//echo "\n totConexoesAtivas do usuario:".$uid." = ".$totConexoesAtivas;
		// e se n�o existe mais nemhum socket conectado, avisa os listeners deste usuario que ele n�o est� online em nada mais
		if($totConexoesAtivas <= 0){
			wsOnUserDisconnect($uid);
		}		
	
}


// retorna totan conexoes ativas
function getTotalActiveConnections($userID){
	$tot = 0;
	$clientIDs = getClientIDSByUser($userID);
	if($clientIDs){
		$tot = count($clientIDs);
	}

	return $tot;
}




/**
*
*  Chamadas dos eventos relativos � este cliente
*
**/

/**
* Chamado quando um usuario se conecta
*/
function wsOnUserConnect($user){

	
	// chama implementa��o
	//onUserConnect($user,$sessao);
	
	//echo "\n wsOnUserConnect(user=".$user.")";
	$arraction = array("action_id"=>newActionID(GOD),
						"type"=>CONTACT_CONNECTING,
						"sender"=>GOD,
						"message"=>"O usuario ".$user." se conectou.",
						"user_connecting" => $user);			
						
	// converto para uma string
	$action = json_encode($arraction,true);
	global $Server;
	//$Server->log("onuserconect:".$user);
	sendActionToUserContacts($user,$action);
	//echo "\n action:".$action;
	// chama implementa��o
	//onUserConnect($user,$sessao);


}

/**
* Chamado quando um usuario se desconecta
*/
function wsOnUserDisconnect($user){
	global $relUsuariosVsLatitudeLongitude;
	global $usuariosConectados;
	
	$arraction = array("action_id"=>newActionID(GOD),
						"type"=>CONTACT_DISCONNECTING,
						"sender"=>GOD,
						"message"=>"O usuario ".$user." se desconectou.",
						"user_disconnecting" => $user);			
						
	// converto para uma string
	$action = json_encode($arraction,true);
	global $Server;
	$Server->log("onuserdisconnect:".$user);
	//echo "\n action:".$action;
	sendActionToUserContacts($user,$action);
	
	// chama a implementa��o do listener
	onUserDisconnect($user);

}

/**
* Chamado quando um usu�rio envia uma mensagem para algu�m ( incluindo o servidor )
*/
function wsOnUserSendMessage($action, $sender, $receiver, $message){
	/*
	* Registra o usuario que est� envando ao seu respectivo socket,
	* teria feito isso no evento onopen do websocket, mas n�o seria possivel
	* o envio de mensagens devido  ao seu readyState ser CONNECTING
	*/
	// alerta os contatos do usuario que esta se conectando
	//alertContactsUserIsConnecting($sender);
	// bindando o usuario ao clientid
	// envia mensagem para o osusario
	sendActionToUser($action,$sender,$receiver);	


}








/**
*
*  Bindando os eventos
*
**/

// Um novo servidor � criado
$Server = new PHPWebSocket();

// E para a alegria de seu deux, avisamos a Ele quem s�o as pessoas respons�veis por cada processo

// Recebe as mensagens enviadas do servidor e as tratamos como Actions
$Server->bind('message', 'wsOnReceiveAction');

// Quando uma nova conex�o � estabelecida
//$Server->bind('onNewClient', 'wsOnNewClient');

// Quando uma conex�o � eliminada
$Server->bind('onClientDisconnect', 'wsOnClientDisconnect');





// for other computers to connect, you will probably need to change this to your LAN IP or external IP,
// alternatively use: gethostbyaddr(gethostbyname($_SERVER['SERVER_NAME']))
$Server->wsStartServer('192.168.1.4',843);


?>
