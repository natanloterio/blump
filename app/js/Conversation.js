function Conversation(id, nome, us) {
    this.id = id;
	this.nome = nome;
	this.us = us;
	this.box = createNewWindow();
	this.size = 0;

}
 
Conversation.prototype.createNewWindow = function() {
	
	var html_add    = "<div class=\"window\" id=\"win_"+this.us+"\">";
		   html_add += "<div class=\"top\" id=\"+id+\"><span>"+this.nome+"</span><a href=\"javascript:void(0);\" id=\"close\">X</a></div>";
		   html_add += "<div id=\"corpo\">";
		   html_add += "<div class=\"mensagens\">";
		   html_add += "<ul class=\"listar\" id=\""+this.id+"\" pagemessages=\"1\"></ul>";
		   html_add += "</div>";
		   html_add += "<input type=\"text\" class=\"mensagem\" id=\""+this.id+"\" us=\""+this.us+"\" maxlength=\"255\" />";
		   html_add += "</div>";
		   html_add += "</div>\"";

    return html_add;
};

Conversation.prototype.getWindow = function() {
	
	return  $("#win_"+this.us);


};


Conversation.prototype.destroyWindow = function() {
	
	$("#win_"+this.us).remove();
	
}

Conversation.prototype.minimize = function() {
	
	var corpo = $("#win_"+this.us).find('#corpo');
	if(corpo){
		this.size = 	corpo.height() ;
		corpo.height("0px");
	}
	
	
}

Conversation.prototype.maxmimize = function() {
	
	var corpo = $("#win_"+this.us).find('#corpo');
	if(corpo){
		
		corpo.height(this.size);
	}
	
	
}


Conversation.prototype.isMaxmimized = function() {
	max= false;
	var corpo = $("#win_"+this.us).find('#corpo');
	if(corpo){
		max = corpo.outerHeight() > 0;
		
	}
	
	return max;

}