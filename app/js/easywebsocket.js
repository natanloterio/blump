var EasyWebSocket = function(url,usrID)
{	
	var _self = this;
	var userID = usrID
	var nick = 'anonymous';
	var senha = null;
	var callbacks = {};
	var ws_url = url;
	var conn;
	var onOpen;
	var sessao;
	var ultimoBlump;
	
	/**
	 *  Constantes dos evnetos 
	 */
	var GOD = 1; // Eu sou Deuxxxxx uaaahahaaa
	// CHAVE DE CRIPTOGRAFIA DO ACTION ID
	var ENCODING_KEY = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	//Cliente est� se conectando
	var USER_CONNECTING = 13;
	// Usu�rio est� se autenticando
	var USER_AUTENTICATION = 14;
	//Usu�rio est� desconectando
	var USER_DISCONNECTING = 15;
	// Mensagem para usuario
	var MESSAGE_TO_USER = 16;
	// Um usuario mudou de status
	var USER_STATE_CHANGE = 17;
	// O usuario nao recebeu a mensagem
	var MESSAGE_NOT_SENT_TO_USER = 23;	
	// O Contato est� se conectando
	var CONTACT_CONNECTING = 18;
	// O Contato est� se desconectando
	var CONTACT_DISCONNECTING = 22;
	// Eventos gerados pelo usuario
	var USER_INTERN_ACTION = 24;
	// registrando sua localizacao
	var UPDATE_USER_LOCATION = 20;
	// registrando sua localizacao
	var WHO_IS_HERE = 25;
	
	var BLUMP = 26;
	// Mudar o username
	var UPDATE_NICKNAME = 27;

	
	function newActionId() {
		//var length = 16;
	    //var result = '';
	    //var chars = ENCODING_KEY;
	    //for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
	    return userID+Date.parse(new Date());
	}
	
	this.updateNick = function(newNick){
			
		var action_id = newActionId(userID);
		var action = {
					"action_id":action_id,
					"type":UPDATE_NICKNAME,
					"sender":this.getUserID() ,
					"receiver":GOD,
					"newNick":newNick
					};
		this.conn.send( JSON.stringify(action,null));		
		
	}
	
	this.bind = function(event_name, callback){
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;// chainable
	};

	
	
	this.onOpen;
	this.onClose;

	this.blump = function(blump_original,message){
		
		
		// montamos o cabecalho da acao
		var action_id = newActionId(userID);
		var action = {
					"action_id":action_id,
					"blump_original":blump_original,
					"type":BLUMP,
					"sender":_self.getUserID() ,
					"nick":_self.getNick(),
					"receiver":GOD,
					"message":message
					};
		this.conn.send( JSON.stringify(action,null));	
		ultimoBlump = action_id;
	}
	
	
	
	this.setNick = function(newNick){
		nick = newNick;
		dispatch('afterChangeNick',newNick);
	
	}
	
	this.getNick = function(){
		return nick;
	
	}
	
	this.getUserID = function(){
		return userID;
	}
	// vamos implementar os eventos de que precisamos algum retorno por parte do nosso cliente
	this.messageToUser =  function(receiverID, message){
		
		// montamos o cabecalho da acao
		var action_id = newActionId();
		var action = {
					"action_id":action_id,
					"type":MESSAGE_TO_USER,
					"sender":this.getUserID(),
					"receiver":receiverID,
					"message":message
					};
		this.conn.send( JSON.stringify(action,null));
		
	}
	
	
	
//	 funcao para atulaizar status do usuario
	this.userConnecting = function(){

		dispatch('afterConnectionEstabilished','');		
		var action_id = newActionId();
		var action = {
					"action_id":action_id,
					"type":USER_CONNECTING,
					"sender":this.getUserID(),
					"nick":nick,
					"receiver":GOD
					};	
	
		if(this.conn.readyState == WebSocket.OPEN){
			this.conn.send( JSON.stringify(action,null));
		}
		
		// limpa a senha
		//this.senha = null;
	}
	
	// pede do servidor um array com posi��es das pessoas dentro de dos limites sw e ne
	this.whoIsHere = function(ne,sw){
		bundle = {	"action_id":newActionId(userID),
					"type":WHO_IS_HERE, 
					"sender":this.getUserID(),
					"receiver":GOD,
					"sw":sw,
					"ne":ne}
		this.conn.send(JSON.stringify(bundle,null));		
	}
	
	// funcao para atualizar posicao do usuario
	this.updateUserLocation = function(position){

		bundle = {	"action_id":newActionId(userID),
					"type":UPDATE_USER_LOCATION, 
					"sender":this.getUserID(),
					"receiver":GOD,
					"latitude":position.coords.latitude,
					"longitude":position.coords.longitude};
		
		//verifica se a conex�o est� estabelecida
		if (this.conn.readyState == this.conn.OPEN) {
			this.conn.send(JSON.stringify(bundle,null));
		}

	}	
	
	this.connect = function() {	
	
		if ( typeof(MozWebSocket) == 'function' )
			this.conn = new MozWebSocket(url);
		else
			this.conn = new WebSocket(url);

		
		
		// Esse carinha aqui � que recebe todos os actions do servidor... man�eee.. gl� gl� gl� gl� gl� 
		this.conn.onmessage = function(evt){
			var action = JSON.parse(evt.data);
			// verifico o tipo da action
			switch(action.type)
			{
			case MESSAGE_TO_USER:
				dispatch('onReceiveMessage',action);
			  break;
			case CONTACT_CONNECTING:
			    dispatch('onContactConnecting',action.user_connecting);
			  break;
			case CONTACT_DISCONNECTING :
				dispatch('onContactDisconnecting',action.user_disconnecting);
			  break;
			case USER_INTERN_ACTION :
				dispatch('onUserInternAction',action.user_inter_action);
			  break;			  
			case WHO_IS_HERE :
				dispatch('onUpdateUseresHere',action.people_arround);
			  break;
			case UPDATE_NICKNAME :
			  _self.setNick(action.newnick);
			  break;
			case BLUMP:
			    if(ultimoBlump !== action.action_id)
				dispatch('onReceiveBlump',action);			
			 break;
			case MESSAGE_NOT_SENT_TO_USER:
			  receiver = action.data.receiver;
			  message = action.data.message;
			  //this.messageNotSentToUser(receiver,message);
			  break;
			default:
			  break;
			  //code to be executed if n is different from case 1 and 2
			}			
			
			}

		/**
		* Aten��o, o estado da conex�o ainda � WebSocket.CONNECTING, ou seja,
		* ainda n�o est� apta a enviar mensagens para o servidor
		*/
		this.conn.onopen = function(){
			_self.userConnecting();
		}
		
		this.conn.onclose = function(){
			dispatch('onCloseConnection');
		}
			
		this.disconnect = function() {
			
			_self.conn.close();
		}	
		

		
	}
	
	/**
	* chamado quando um contato se conecta
	*/
	this.onContactConnect = function(contactID){}

	/**
	* chamado quando um contato se desconecta
	*/
	this.onContactDisconnect = function(contactID){}
	
	/**
	* chamado quando um contato se desconecta
	*/
	this.onReceiveMessage = function(senderID, message){}
	
	/**
	* chamado quando uma mensagem n�o tenha conseguido chegar no destino
	*/
	this.messageNotSentToUser = function(receiverID, message){}	
	
	
	

	
	var dispatch = function(event_name, message){
		var chain = callbacks[event_name];
		if(typeof chain == 'undefined') return; // no callbacks for this event
		for(var i = 0; i < chain.length; i++){
			chain[i]( message );
		}
	}
}