<html>
    
</html>
<style>
    
    
.primary-1 { background-color: #40798B }
.primary-2 { background-color: #406C7A }
.primary-3 { background-color: #286073 }
.primary-4 { background-color: #5995A8 }
.primary-5 { background-color: #6297A8 }

.secondary-a-1 { background-color: #4E5B98 }
.secondary-a-2 { background-color: #4C5685 }
.secondary-a-3 { background-color: #303E7E }
.secondary-a-4 { background-color: #6573B2 }
.secondary-a-5 { background-color: #6E7AB2 }

.secondary-b-1 { background-color: #48A367 }
.secondary-b-2 { background-color: #498F61 }
.secondary-b-3 { background-color: #2C874B }
.secondary-b-4 { background-color: #5FBA7E }
.secondary-b-5 { background-color: #69BA85 }

.complement-1 { background-color: #DFA262 }
.complement-2 { background-color: #C39564 }
.complement-3 { background-color: #B87C3D }
.complement-4 { background-color: #E7B076 }
.complement-5 { background-color: #E7B683 }

    
           .feedList{
                background-color: #eceaea;
                list-style:none;
                padding:10px;
                display:inline-block;
            }

            .feedList li{
                margin-bottom: 10px;
            }

            .feedList li .avatar{
                max-width:100px;
                background-color:#000;
                display:inline-block;
                margin-right:5px;
            }

            .feedList li .content{
                display:inline-block;
            }

            .feedList li .content h6{
                font-size:18px;
                margin:0;
                margin-bottom:10px;
                color: #4b4b4b;
            }
            
</style>
<body>
  <div>
    <ul class="feedList">
            <li>
                <img class="avatar" src="blank-avatar.png" />
                <div class="content">
                    <h6>ttocskcaj</h6>
                    <span>New Topic: <a href="#">My awesome post!</a></span>
                </div>
            </li>
            
            
            <li>
                <img class="avatar" src="blank-avatar.png" />
                <div class="content">
                    <h6>ttocskcaj</h6>
                    <span>New Topic: <a href="#">My awesome post!</a></span>
                </div>
            </li>
            
            
            <li>
                <img class="avatar" src="blank-avatar.png" />
                <div class="content">
                    <h6>ttocskcaj</h6>
                    <span>New Topic: <a href="#">My awesome post!</a></span>
                </div>
            </li>
            
            
            <li>
                <img class="avatar" src="blank-avatar.png" />
                <div class="content">
                    <h6>ttocskcaj</h6>
                    <span>New Topic: <a href="#">My awesome post!</a></span>
                </div>
            </li>

        </ul>
    </div>
</body>
</html>
