<?php
//error_reporting(E_ALL);
/*
 * Este arquivo implementa o protocolo de comunica��o do websocket
 * Os pacotes recebidos s�o chamados de Actions. Essas actions contem os seguintes
 * atributos:
 *
 *  action_id : contem uma chave unica para cada action (alfanumerico)
 *  type : tipo da action. Vide {@action}
 *  time : data de criacao do pacote no formato numerico sequencial
 *  sender : user_id de quem envia
 *  receiver : user_id do destinat�rio
 *  data : pacote contendo a mensagem
 *
 *  Abaixo segue um modelo de um Action em JSON
 *
 *
 *  {
 *  	action_id : df877899df,
 *  	type : USER_MESSAGE,
 *  	time : 1368231308432,
 *  	sender : df09234234,
 *  	receiver : kl44334434,
 *  	data : {msg:"Oi"}
 *
 *  }
 */
include_once("ServerInterface.php");

//mostra();
// prevent the server from timing out
set_time_limit(0);

// include the web sockets server script (the server is started at the far bottom of this file)
require 'class.PHPWebSocket.php';

// array com a relacao usuario x socket
$usuariosConectados = array();
// array com o relacionamento clientID x UserID
$relClientIDvsUserID = array();
$relUserIDvsClientID = array();
$relUsuarioVsContato = array();
$maxUsers = 0;
$maxSockets = 0;

// Matriz com o ID do usu�rio e os sockets deste.
//$relUserIDvsClientID = array();


/**
 *  Constantes dos evnetos
 */
// Esse meu querido, � Deuxxxx .. Muaaa-ha-ha!
// thick-thick-thick-thick-thick-thick
const GOD = 1;
//Usuario est� se conectando
const USER_CONNECTING =13;
// Usu�rio est� se autenticando
const USER_AUTENTICATION = 14;
//Usu�rio est� desconectando
const USER_DISCONNECTING = 15;
const MESSAGE_TO_USER = 16;
// Um usuario mudou de status
const USER_STATE_CHANGE = 17;
// O usuario nao recebeu a mensagem
const MESSAGE_NOT_SENT_TO_USER = 23;
// O contato est� se conectando
const CONTACT_CONNECTING = 18;
// O contato est� se desconectando
const CONTACT_DISCONNECTING = 22;	
// registrando sua localizacao
const REGISTER_POSITION = 20;
// adiciona um contato
const ADD_CONTACT = 21;
// Eventos gerados pelo usuario
const USER_INTERN_ACTION = 24;
// Usuario logou
const LOGIN_SUCESS = 25;

/**PORTA DE ENTRADA DO SERVIDOR
 * Quando um cliente envia uma action para o server, lemos o cabecalho dessa action
 * e efetuamos a action que nela est� contida
 */
function wsOnReceiveAction($clientID, $action, $messageLength, $binary) {
	global $Server;
	$Server->log("Servidor Recebeu A Action : \n".identaAction($action));
	// intercepta a mensagem e identifica o usuario nela presente
	$arAction = json_decode($action,true);
	//$Server->log($bundle);
	// pega o userid do sender
	$sender = $arAction['sender'];
	$receiver = $arAction['receiver'];	

	
	
	// vincula um userid a um client id
	setUserIDByClientID($clientID, $sender);
	
	// tipo do evento
	$type = $arAction["type"];
	
	echo "\n receiver:".$receiver."\n"." e type :".$type;

	// Separo as solicita��es para mim ( GOD ) das destinadas aos outros usuarios
	if($receiver == GOD){
		switch ($type){
			// caso seja um usuario se conectando
			case USER_CONNECTING:
				// agora que sabems que eh um usuario se conectando, esperamos um parametro com o seu cod
				echo "\n Usuario ".$sender . " ta se Conectando...";
				
				// valida senha
				$senha = $arAction['senha'];
				$logado = login($senha,$logado);
				
				if($logado){

					// bindando o usuario ao clientid
					setUserIDByClientID($clientID, $sender);
					// adicionando um clientID a um usuario
					addClientIDToUser( $sender, $clientID);
					// cria uma nova sessao
					$SESSAO = uniqid();
					
					//$totConexoesAtivas = getTotalActiveConnections($sender);
					// dispara evendo de um usuario se conectando				
					wsOnUserConnect($sender,$SESSAO);
					


				
				}
				
				
				
				break;

			case REGISTER_POSITION:
				
				//$Server->log("\n Usuario ".$sender . " ta se Desconectando...");
				break;
		}
	}else{
		/**
		 * Eventos de um usuario para outro
		 */

		// enviamos uma mensagem de volta para o usuario para avisar outros clientes deste mesmo usuario
		wsOnUserIternAction($action);
			  
		// envia mensagem para o osusario
		sendActionToUser($action,$sender,$receiver);				
				
	}

}


function wsOnUserIternAction($action){

	$arAction = json_decode($action,true);
	//$Server->log($bundle);
	// pega o userid do sender
	$sender = $arAction['sender'];
	
	$arrUserInternAction = array("action_id"=>newActionID($sender),
				"type"=>USER_INTERN_ACTION,
				"sender"=>GOD,
				"receiver"=>$sender,
				"user_inter_action"=>$arAction);
	// converto o array para json
	$newaction = json_encode($arrUserInternAction,true);		
	sendActionToUser($newaction,GOD,$sender);

}

// when a client connects
function wsOnOpen($clientID){
	// 	global $Server;

}

// when a client closes or lost connection
function wsOnClose($clientID, $closeStatus) {
	// chamando onClose
	echo "\n wsOnClose chamado";
	global $Server;
	// antes de remover o client, dispara um evento de desconxao do usuario deste ID
	$userid = getUserIDByClientID($clientID);

	$Server->log("\n fechando conexao com o cliente ".$clientID." do usuario ".$userid);
	// chamando evento de mudan�a de status de usuario

}

function addContactToUser($user,$contact){
	
	global $relUsuarioVsContato;
	$relUsuarioVsContato[$user][$contact] = $contact;
	
	
}

function updateFollowersList($user){
	
	$xlist = getFriendsList($user);
	
	foreach ( $xlist  as $follower){	

		addContactToUser($user,$follower);
	}
	
}

/**
* Retorna um array com os UserID's dos usuarios a serem notificados pelo usuario passado por parametro
*
*/
function getUserFollowers($user){
	global $relUsuarioVsContato;

	if(array_key_exists($user,$relUsuarioVsContato))	{
	
		return $relUsuarioVsContato[$user];
	
	}else{
		updateFollowersList($user);
		return $relUsuarioVsContato[$user];
	}
}

/**
 *
 * Retorna um novo e unico ActionID
 * @param unknown_type $userID
 */
function newActionID($userID){

	$milliseconds = round(microtime(true) * 1000);
	$actionID = $userID.$milliseconds;
	return $actionID;
}

/**
* Envia um action para os contatos de uma pessoa
*
*
*/
function sendActionToUserContacts($sender,$action){
	echo "\n sendActionToUserContacts(sender=".$sender.") \n";
	$contatosDoUsuario = getUserFollowers($sender);
	$arrAction = json_decode($action,true);
	global $Server;
	
	// se a lista retornar alguma coisa
	if($contatosDoUsuario){
	
	foreach ( $contatosDoUsuario  as $contato){
			$arrAction['receiver'] = $contato;
			$act = json_encode($arrAction,true);
			// envia o action para o contato, =) simples !
			//echo "\n avisando contato (".$contato.") que usuario :".$sender." enviou action: ".$action."\n";
			sendActionToUser($act,$sender,$contato);
			
	
	
		}
	
	
	}else{
		$arrAction = array("action_id"=>newActionID(GOD),
						"type"=>MESSAGE_NOT_SENT_TO_USER,
						"sender"=>GOD,
						"receiver"=>$sender,
						"message"=>"Usuario n�o possui contatos online.",
						"data"=>$action);
		// converto o array para json
		$newaction = json_encode($arrAction,true);		
		sendActionToUser($newaction,GOD,$sender);
		$Server->log("Nao foi possivel enviar o action:".action);
	}
	
}


/**
* Soh funciona para usuarios que se autenticaram no servidor ( bug )
*
*/
function sendActionToUser($action, $sender, $receiver){
	global $Server;
	$Server->log("$sender enviou a action para $receiver : \n".identaAction($action));
	$arAction = json_decode($action,true);
	$type = $arAction['type'];
	// Se as mensagens sao destinadas � Mim, as intercepto aqui

		// pego uma lista com todos os ids relacionados a este usuario
		$recClientIDs = getClientIDSByUser($receiver);
		//$Server->log("Receiver :".$receiver);
		//$Server->log("recClientIDS :".$recClientIDs);
		// se o usuario possui alguns sockets abertos, enviamos para todos eles a mensagem
		if($recClientIDs){
			//$Server->log("passou no if recClientIDS");
			// e fa�o um loop enviando uma copia da mensagem para cada um deles ;) sussi
			foreach ( $recClientIDs as $id ){
					$Server->wsSend($id, $action);
			}

		}else{

			$arrAction = array("action_id"=>newActionID(GOD),
							"type"=>MESSAGE_NOT_SENT_TO_USER,
							"sender"=>GOD,
							"receiver"=>$sender,
							"message"=>"O usuario estava offline.",
							"data"=>$action);
			// converto o array para json
			$action = json_encode($arrAction,true);
			// envio esse json de volta para o usuario que tentou enviar a mensagem
			$recClientIDs = getClientIDSByUser($sender);
			if($recClientIDs){
				// e fa�o um loop enviando uma copia da mensagem para cada um deles ;) sussi
				foreach ( $Server->wsClients as $id => $client )
					$Server->wsSend($id, $action);
			}
		}
	

}

function addClientIDToUser($UserID, $IDSocket){

	//echo "\n adicionando o ClientID $IDSocket ao usuario $UserID";

	global $relUserIDvsClientID;
	global $Server;
	global $xCountUsers;
	global $xCountSockets ;
	global $maxUsers;
	global $maxSockets;
	
	$relUserIDvsClientID[$UserID][$IDSocket] =  $IDSocket;

	$xCountUsers = count($relUserIDvsClientID);
	$xCountSockets = (count($relUserIDvsClientID ,COUNT_RECURSIVE) - count($relUserIDvsClientID, 0));

	if ($xCountUsers > $maxUsers)
	$maxUsers = $xCountUsers;
	if ($xCountSockets > $maxSockets	)
	$maxSockets = $xCountSockets;

	$Server->log('Usuarios conectados  : '. $xCountUsers);
	$Server->log('Sockets instanciados : '. $xCountSockets);
//	$Server->log('Maximo de usuarios conectados : '. $maxUsers);
//	$Server->log('Maximo de sockets instanciados: '.$maxSockets);

	$Server->log('');
}

function removeClientIDFromUser($userID,$clientID){
	global $Server;
	global $relUserIDvsClientID;
	if(array_key_exists($userID,$relUserIDvsClientID)){
		unset($relUserIDvsClientID[$userID][$clientID]);
		//$Server->log("primeiro id do usuario ".$userID." = ".$cliIDS);
	}
	

}

function removeUserFromClientIDs($clientID){
	global $relClientIDvsUserID;
	unset($relClientIDvsUserID[$clientID]);
}

// retorna um array com a lista dos clientIDs de um usuario
function getClientIDSByUser($userid){
	global $Server;
	global $relUserIDvsClientID;
	$cliIDS = array();
	// returnoando um array de clientsIDs
	//$Server->log("getClientIDSByUser(".$userid.");");
	if(array_key_exists($userid,$relUserIDvsClientID)){
		$cliIDS = $relUserIDvsClientID[$userid];
		//$Server->log("primeiro id do usuario ".$userid." = ".$cliIDS);
	}
	return $cliIDS;
}



function getUserIDByClientID($clientID){
	global $relClientIDvsUserID;
	global $Server;
	$uid = $relClientIDvsUserID[$clientID];
	//$Server->log("\n relClientIDvsUserID[".$clientID."] = ".$relClientIDvsUserID[$clientID]);
	return $uid;
}

function setUserIDByClientID($clientID, $userID){
	global $relClientIDvsUserID;
	global $Server;
	//$Server->log("\n setUserIDByClientID(".$clientID.",".$userID.");");
	$relClientIDvsUserID[$clientID] = $userID;
}


function identaAction($action){
	
	$arrAction = json_decode(strval($action),true);
	

	$actionIdentado = "\n ======== ACTION[".$arrAction['action_id']."]================\n";
	foreach(array_keys($arrAction) as $paramName){
	  $actionIdentado .="||  ".$paramName . ":".$arrAction[$paramName]."\n";	
	}
	
	$actionIdentado .= "\n =========================================== \n";
	
	return $actionIdentado;

}
/**
 *
 * Chamado antes de chamar o evento onClose no cliente
 * @param unknown_type $clientID
 * @param unknown_type $status
 */
function wsOnClientDisconnect($clientID,$status){
	//echo "wsOnClientDisconnect(clientID=".$clientID.",".$status.")";
	// descobre o codigo do usuario
	$uid = getUserIDByClientID($clientID);
	//echo "\n userID =".$uid." e clientID=".$clientID."\n";
	
	// remove este socket da lista
	removeClientIDFromUser($uid,$clientID);
	removeUserFromClientIDs($clientID);
	// verifica se ainda possuimos algum outro socket conectado a esse usuario
	$totConexoesAtivas = getTotalActiveConnections($uid);
	//echo "\n totConexoesAtivas do usuario:".$uid." = ".$totConexoesAtivas;
	// e se n�o existe mais nemhum socket conectado, avisa os listeners deste usuario que ele n�o est� online em nada mais
	if($totConexoesAtivas <= 0){
		wsOnUserDisconnect($uid);
	}		

}


// retorna totan conexoes ativas
function getTotalActiveConnections($userID){
	$tot = 0;
	$clientIDs = getClientIDSByUser($userID);
	if($clientIDs){
		$tot = count($clientIDs);
	}

	return $tot;
}




/**
*
*  Chamadas dos eventos relativos � este cliente
*
**/

/**
* Chamado quando um usuario se conecta
*/
function wsOnUserConnect($user,$sessao){

	// responde o usuario que est� se conectando que o login foi bem sucedido.
	$arraction = array("action_id"=>newActionID(GOD),
						"type"=>LOGIN_SUCESS,
						"sender"=>GOD,
						"logged"=>1,
						"sessao"=>$sessao);			
						
	// converto para uma string
	$action = json_encode($arraction,true);

	//$Server->log("onuserconect:".$user);
	sendActionToUser($user,$action);	

	// chama implementa��o
	onUserConnect($user,$sessao);

	echo "\n wsOnUserConnect(user=".$user.")";
	$arraction = array("action_id"=>newActionID(GOD),
						"type"=>CONTACT_CONNECTING,
						"sender"=>GOD,
						"message"=>"O usuario ".$user." se conectou.",
						"user_connecting" => $user);			
						
	// converto para uma string
	$action = json_encode($arraction,true);
	global $Server;
	//$Server->log("onuserconect:".$user);
	sendActionToUserContacts($user,$action);
	//echo "\n action:".$action;



}

/**
* Chamado quando um usuario se desconecta
*/
function wsOnUserDisconnect($user){

	$arraction = array("action_id"=>newActionID(GOD),
						"type"=>CONTACT_DISCONNECTING,
						"sender"=>GOD,
						"message"=>"O usuario ".$user." se desconectou.",
						"user_disconnecting" => $user);			
						
	// converto para uma string
	$action = json_encode($arraction,true);
	global $Server;
	$Server->log("onuserdisconnect:".$user);
	//echo "\n action:".$action;
	sendActionToUserContacts($user,$action);
	
	// chama a implementa��o do listener
	onUserDisconnect($user);

}

/**
* Chamado quando um usu�rio envia uma mensagem para algu�m ( incluindo o servidor )
*/
function wsOnUserSendMessage($action, $sender, $receiver, $message){
	/*
	* Registra o usuario que est� envando ao seu respectivo socket,
	* teria feito isso no evento onopen do websocket, mas n�o seria possivel
	* o envio de mensagens devido  ao seu readyState ser CONNECTING
	*/
	// alerta os contatos do usuario que esta se conectando
	//alertContactsUserIsConnecting($sender);
	// bindando o usuario ao clientid
	// envia mensagem para o osusario
	sendActionToUser($action,$sender,$receiver);	


}



/**
*
*  Bindando os eventos
*
**/

// Um novo servidor � criado
$Server = new PHPWebSocket();

// E para a alegria de seu deux, avisamos a Ele quem s�o as pessoas respons�veis por cada processo

// Recebe as mensagens enviadas do servidor e as tratamos como Actions
$Server->bind('message', 'wsOnReceiveAction');

// Quando uma nova conex�o � estabelecida
//$Server->bind('onNewClient', 'wsOnNewClient');

// Quando uma conex�o � eliminada
$Server->bind('onClientDisconnect', 'wsOnClientDisconnect');





// for other computers to connect, you will probably need to change this to your LAN IP or external IP,
// alternatively use: gethostbyaddr(gethostbyname($_SERVER['SERVER_NAME']))
$Server->wsStartServer('192.168.1.5', 843);

?>
